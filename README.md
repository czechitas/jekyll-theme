# jekyll-theme-czechitas

Reponzivní téma a Jekyll konfigurace, která ti umožní jednoduše postavit a udržovat web [jako tento](http://start.czechitas.cz) jen psaním Markdown souborů.

![](screenshot.png)

## Instalace

1. [Nainstaluj si Ruby](https://www.ruby-lang.org/en/documentation/installation/)

2. Nainstaluj si Bundler:

   ```sh
   gem install bundler
   ```

3. Stáhni si jako základ třeba [zdrojáky webu Základy programování](https://gitlab.com/czechitas/kurz_start/repository/archive.zip?ref=master).

4. V jeho adresáři pak dej stáhnout závislosti:

   ```sh
   cd kurz_muj
   bundle install
   ```

## Použití

Hlavní co potřebuješ je do adresáře `_lessons` vkládat své lekce (název jen musí končit na `.md`, viz. existující web) a v `_config.yml` upravit své specifické nastavení.

Na svém počítači pak web vyzkoušíš takto:

```sh
bundle exec jekyll s
```

Spustí se lokální webserver, kde si můžeš sestavený web prohlédnout na: <http://localhost:4000>

Lekce se řadí podle data v hlavičce (tedy nezáleží na názvu souboru) s tím, že datum v budoucnu Jekyll ignoruje (aby bylo možné psát lekce dopředu). Pokud si chceš prohlédnout web i s budoucími lekcemi, tak jej spusť takto:

```sh
bundle exec jekyll s --future
```

Verzi Czechitas tématu, Jekyllu i všech závislostí aktualizuješ takto:

```sh
bundle update
```

## GitLab Pages


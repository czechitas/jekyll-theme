# coding: utf-8

Gem::Specification.new do |spec|
  spec.name          = "jekyll-theme-czechitas"
  spec.version       = "0.3"
  spec.authors       = ["Honza Pobořil"]
  spec.email         = ["honza@poboril.cz"]

  spec.summary       = %q{Jekyll theme for courses support website made for Czechitas.}
  spec.homepage      = "https://gitlab.com/czechitas/jekyll-theme"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r{^(assets|_layouts|_includes|_sass|LICENSE|README)}i) }
  spec.metadata["plugin_type"] = "theme"

  spec.add_development_dependency "jekyll", "~> 3.3"
  spec.add_development_dependency "bundler", "~> 1.12"
  spec.add_development_dependency "jekyll-extlinks", '~> 0'
end
